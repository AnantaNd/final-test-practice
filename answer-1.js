const values = {
    name: "John Doe",
    age: 21,
    username: "johndoe",
    password: "abc123",
    location: "Jakarta",
    time: new Date(), 
}


function replace(obj, keys)
{
    var dup = {};
    for (var key in obj) {
        if (keys.indexOf(key) == -1) {
            dup[key] = obj[key];
        }
    }
    return dup;
}

const json = JSON.stringify(replace(values, ['password', 'time']));
console.log(json);